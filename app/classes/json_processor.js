'use strict';
var json_processor = {

	filter_concat: function (json_obj, type, workflow) {
		var json_array = [];
		//Foreach would look better but has performance consequences.
    for (var i = 0; i < json_obj.length; i++) {
			//filter by type htv and completed workflow and create new json object
			var obj = json_obj[i];
			if ((obj.type == type) && (obj.workflow == workflow)) {
				var jsonObjConcat = {};
				var add = obj.address;
				if (add.unitNumber === undefined) {
					jsonObjConcat.concataddress = add.buildingNumber + ' ' + add.street + ' ' + add.suburb + ' ' + add.state + ' ' + add.suburb;
				} else {
					jsonObjConcat.concataddress = add.unitNumber + '/' + add.buildingNumber + ' ' + add.street + ' ' + add.suburb + ' ' + add.state + ' ' + add.suburb;
				}
				jsonObjConcat.type = type;
				jsonObjConcat.workflow = workflow;
				json_array.push(jsonObjConcat);
			}
		}

		return json_array;

	}

};

module.exports = json_processor;
