var express = require('express');
var router = express.Router();

//----------------------------------------custom modules
var json_process = require('../classes/json_processor');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('Home Track Link test');
});

router.post('/', function(req, res, next) {
  var json         = req.body.payload;
  var jsonObj      = {};

  jsonObj.response = json_process.filter_concat(json, 'htv', 'completed');
  res.status(200).send(jsonObj);
  
});
module.exports = router;
