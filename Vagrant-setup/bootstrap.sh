#!/bin/sh -ex


# Update package list and upgrade all packages
#sudo yum -y update
sudo yum -y install epel-release

sudo yum -y install nodejs npm telnet ntp 

cd /vagrant/app

#npm install dependencies for node
npm install

#Set timezone
sudo rm /etc/localtime
sudo ln -s /usr/share/zoneinfo/Australia/Sydney /etc/localtime
